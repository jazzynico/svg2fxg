## Summary

The goal of this extension was originally to provide an FXG format export for Inkscape (since Adobe abandoned SVG support in its Flash applications, Flash developer needed a way to create their drawings with Inkscape).

The file is now available as a separate project.

## Usage

### As an Inkscape extension

SVG to FXG is already available in Inkscape (0.48+) in the *File > Save as menu*.

If your Inkscape version is too old, you can replace the installed version of the XSL file in the share/extensions folder if the Inkscape application with a newer file downloaded from the gitlab project.

### As a standalone XSL

First install xsltproc and run it from a command line interface:
> xsltproc svg2fxg.xsl ORIGINALFILE.svg > FXGFILE.fxg
